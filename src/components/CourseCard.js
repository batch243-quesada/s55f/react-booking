import React, { useContext, useEffect, useState } from 'react'
import { Button, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

const CourseCard = ({course}) => {
  const [quantity, setQuantity] = useState(0);
  const [slots, setSlots] = useState(course.slots);
  // console.log(course.enrollees)

  const [isAvailable, setIsAvailable] = useState(true);

  const { user } = useContext(UserContext);

  useEffect(() => {
    // console.log("rendering");
    slots === 0 && setIsAvailable(false);
    // console.log(isAvailable)
  }, [isAvailable, slots])

  const handleEnroll = () => {
    quantity < 30 && setQuantity(quantity + 1)
    slots > 0 && setSlots(slots - 1);
    if(slots === 1) {
      alert('Congrats!');
    }
  }

  return (
    <Card className='h-100'>
      <Card.Body>
        <Card.Title>{course.name}</Card.Title>
        <Card.Subtitle className='cardSub'>Description:</Card.Subtitle>
        <Card.Text>{course.description}</Card.Text>
          <div className='price-enrollees-container'>
            <Card.Subtitle className='cardSub'>Price: &#8369; {course.price}</Card.Subtitle>
            <Card.Subtitle className='cardSub'>Enrollees: {quantity}</Card.Subtitle>
            <Card.Subtitle className='cardSub'>Slots: {slots} slots</Card.Subtitle>

          </div>
          {
            (!user)
            ? <Button as={Link} to='/login' variant="primary" onClick={handleEnroll}>Enroll</Button>
            : (slots === 0)
            ? <Button variant="secondary" disabled>Full</Button>
            : <Button as={Link} to={`/courses/${course._id}`} variant="primary">Details</Button>
          }
      </Card.Body>
    </Card>
  )
}

export default CourseCard