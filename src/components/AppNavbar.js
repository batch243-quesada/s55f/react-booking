// modules
import React, { useContext } from 'react'
import { Container, Nav, Navbar } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

const AppNavbar = () => {
  //store the user info from the localStorage
  // const [user, setUser] = useState(localStorage.getItem("email"));
  const {user} = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg" className='navbar-container vw-100'>
      <Container fluid>
        <Navbar.Brand as = {NavLink} to="/">Course Booking</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as = {NavLink} to="/">Home</Nav.Link>
            <Nav.Link as = {NavLink} to="/courses">Courses</Nav.Link>
            {
              (user.id !== null)
              ? <>
                <Nav.Link as = {NavLink} to="/logout">Logout</Nav.Link>
                </>
              : <>
                <Nav.Link as = {NavLink} to="/register">Register</Nav.Link>
                <Nav.Link as = {NavLink} to="/login">Login</Nav.Link>
                </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default AppNavbar