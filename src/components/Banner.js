// modules
import React from 'react'
import { Col, Row, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Banner = () => {
  return (
    <Row className='mt-3 mb-5'>
        <Col>
            <h1>Zuitt Coding Bootcamp</h1>
            <p>Opportunities for everyone, everywhere.</p>

            <Button as={Link} to='/courses' variant='primary'>ENROLL NOW!</Button>
        </Col>
    </Row>
  )
}

export default Banner