import React from 'react'

// Create a context object
// A context object as the name states is a data type that can be used to store information that can be shared to other components within the app
// The context object is a different approach to passing information between components and allows easier access between components and allows easier access by the use of prop-drilling

// Storing info in a context object is done by providing the info using the corresponding "Provider" component and passing the info via the "value" prop
// All information provided to the Provider component can be accessed later on from the context object as properties

// createContext is a method used to create a context in react
const UserContext = React.createContext();

// The "Provider" component allows other component to consime/use the context object and supply the necessary information needed to the context Object.
export const UserProvider = UserContext.Provider;

export default UserContext;