// import coursesData from "../data/courses";
import CourseCard from "../components/CourseCard";
// import { Col, Container, Row } from "react-bootstrap";
import { useEffect, useState } from "react";

// const local = localStorage.getItem("email")

const Courses = () => {
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_URI}/courses/allActiveCourses`)
    .then(response => response.json())
    .then(data => {
      console.log(data);

      setCourses(data.map(course => {
        return (<CourseCard key={course._id} course={course} />)
      }));
    })
  }, []);

  return (
    // <Container>
    //   <Row className='courseRow w-100'>
    //     {coursesData.map(course => (
    //       <Col className="courseCard offset-md-4" md={4} key={course.id}>
    //           <CourseCard course = {course}/>
    //       </Col>
    //     ))}
    //   </Row>
    // </Container>

    <>
      {courses}
    </>
  )
}

export default Courses