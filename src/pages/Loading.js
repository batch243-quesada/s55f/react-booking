import React from 'react'
import { Link } from 'react-router-dom';

const Loading = () => {
  return (
    <>
        <div className="loader-wrapper">
            <div className="loader-container">
            <span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>

            <div className='loader-text'>
                Loading...
            </div>
            </div>
        </div>
    </>
  )
}

export default Loading