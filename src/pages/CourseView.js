import { useEffect, useState } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";

const CourseView = () => {
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(null); 

    const {courseId} = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        fetch(`${process.env.REACT_APP_URI}/courses/${courseId}`)
        // no need method or token kasi get active products lang to
        .then(response => response.json())
        .then(data => {
            // console.log(data);
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        })
    }, [courseId]);

    const enroll = courseId => {
        fetch(`${process.env.REACT_APP_URI}/users/enroll/${courseId}`, {
            method: 'POST',
            headers: {
                'Content-Type' : "application/json",
                Authorization : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(data === true) {
                Swal.fire({
                    title: "Successfully enrolled!",
                    icon: 'success',
                    text: "Good luck!"
                })

                navigate('/courses');

            } else {
                Swal.fire({
                    title: "Sorry, something went wrong",
                    icon: 'error',
                    text: "Dasurv!"
                })

                navigate('/');
            }
        })
    }

    return (
        <Container>
            <Row>
                <Col lg={{span: 6, offset: 3}}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            <Card.Subtitle>Class Schedule</Card.Subtitle>
                            <Card.Text>8 am - 5 pm</Card.Text>
                            <Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
                        </Card.Body>	
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}

export default CourseView