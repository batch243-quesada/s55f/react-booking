import { Container } from 'react-bootstrap'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

const Home = () => {



  return (
    <>
      <Container className='mb-5 homeContainer'> 
            <Banner />
            <Highlights />
      </Container>
    </>
    
  )
}

export default Home