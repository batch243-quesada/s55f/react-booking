// modules
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Login from './pages/Login';
import Register from './pages/Register';
import Home from './pages/Home';
import Courses from "./pages/Courses";
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import { useEffect, useState } from 'react';
import { UserProvider } from './UserContext';
import CourseView from './pages/CourseView';

const App = () => {
  // state hook for the user that will be globally accessible using useContext
  // will also be used to store the user info and be used for validating if a user is logged in on the app or not
  const [user, setUser] = useState({id: null, isAdmin: false});

  // function for clearing localStorage in the logout
  const unSetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user)
  }, [user])
  
  useEffect(() => {
    fetch(`${process.env.REACT_APP_URI}/users/profile`,
			{headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}})
			.then(response=> response.json())
			.then(data => {
				console.log(data);
				
				setUser({id: data._id, isAdmin: data.isAdmin})

			})	
  },[])

  return (
    <>
         <UserProvider value={{user, setUser, unSetUser}}>
            <Router>
              <AppNavbar />
              <Routes>
                <Route path='/' exact element={<Home/>} />
                <Route path='/login' exact element={<Login/>} />
                <Route path='/register' exact element={<Register/>} />
                <Route path='/courses' exact element={<Courses/>} />
                <Route path='/logout' exact element={<Logout/>} />
                <Route path='/courses/:courseId' element={<CourseView/>} />


                <Route path='*' element={<NotFound/>} />

              </Routes>
          </Router>
          </UserProvider>
    </>
  );
}

export default App;
